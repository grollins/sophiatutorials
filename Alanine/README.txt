1. With Chimera open and Sophia loaded, click New Simulation in the Main Window.

2. In the Universe Panel of the Simulation Window, click "Select PDB File..." and select alanine.pdb.

3. Select Amber94 or Amber99 from the Force Field dropdown menu.

4. Click Load Universe. The alanine molecule will appear in the Chimera window.

5. For a better view, in the Chimera window, click on Presets --> Custom 1 (Sophia Licorice).

6. In the Recipe Panel of the Simulation Window, click the "Load Recipe" button. Select recipe.json.

7. Scroll down to the bottom of the Simulation Window. Click "Run All Ingredients".

8. When the simulation is complete (a few seconds), the trajectory will be loaded for viewing. Use the Trajectory Viewer Window to play the movie. The plot of energy vs. frame will also be displayed.
