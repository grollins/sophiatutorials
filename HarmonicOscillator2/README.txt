1. With Chimera open and Sophia loaded, click New Simulation in the Main Window.

2. In the Universe Panel of the Simulation Window, click "Select PDB File..." and select N2.pdb.

3. Select GAFF from the Force Field dropdown menu.

4. Click the "Select File..." button next to the label that says "FF Mod File (optional)". Select N2.frcmod.

5. Click Load Universe. The N2 molecule will appear in the Chimera window.

6. For a better view, in the Chimera window, click on Actions --> Atoms/Bonds --> Ball & Stick.

7. In the Recipe Panel of the Simulation Window, click the "Load Recipe" button. Select recipe_0.1.json.

8. Scroll down to the bottom of the Simulation Window. Click "Run Next Ingredient" or "Run All Ingredients". Our recipe only has one ingredient, so the buttons are equivalent in this case.

9. When the simulation is complete (a few seconds), the trajectory will be loaded for viewing. Use the Trajectory Viewer Window to play the movie. The plot of energy vs. frame will also be displayed.

10. Repeat steps 1-9 but load a different recipe in step 7. The ten recipes provided differ only in the time step of the MD. The recipes are named "recipe_x.json" where x is the time step in femtoseconds.
