1. With Chimera open and Sophia loaded, click New Simulation in the Main Window.

2. In the Universe Panel of the Simulation Window, click "Select PDB File..." and select argon_pair.pdb.

3. Select Lennard Jones from the Force Field dropdown menu.

4. This step is optional. Click the "Select File..." button next to the label that says "FF Mod File (optional)". Select LJ_argon.mod (the vdw radius and well depth of argon will be modified based on the contents of the .mod file).

5. Click Load Universe. The argon atoms will appear in the Chimera window.

6. For a better view, in the Chimera window, click on Presets --> Custom 2 (Sophia VDW).

7. In the Recipe Panel of the Simulation Window, click the "Load Recipe" button. Select recipe.json.

8. Scroll down to the bottom of the Simulation Window. Click "Run Next Ingredient" or "Run All Ingredients". Our recipe only has one ingredient, so the buttons are equivalent in this case.

9. When the simulation is complete (a few seconds), the trajectory will be loaded for viewing. Use the Trajectory Viewer Window to play the movie. The plot of energy vs. frame will also be displayed.
