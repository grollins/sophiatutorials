Summary of Contents
-------------------

HarmonicOscillator1
Simple oscillator 1000 steps, time step 1 fs

HarmonicOscillator2
Simple oscillator with range of time steps

ArgonPair
MD simulation of two argon atoms (1D)

ArgonTen
MD simulation of ten argon atoms (2D)

Ethane1
Low 1-4 vdw barrier; no explicit torsion

Ethane2
No 1-4 vdw interactions; explicit 3-fold torsion only

Alanine
Minimization, MD heating to 300K, MD at 300K


Format of .mod files
--------------------
Ar, 4.8, 0.3
<atom name>, <well depth (kJ/mol)>, <R_min (nanometers)>

Format of .frcmod files
-----------------------
bond modification format:
<atom names> <force constant> <equilibrium bond length (angstroms)>
example: c3-c3  635.8    1.5400

dihedral modification format:
<atom names> <divide barrier by this quantity> <0.5 * energy barrier (kcal/mol)> <phase shift angle (degrees)> <periodicity>
example: hc-c3-c3-hc   1    0.00          0.0             3.

van der Waals modification format:
<atom name> <vdw radius (angstroms)> <well depth>
example:  c3          1.3000  0.1000
