1. With Chimera open and Sophia loaded, click New Simulation in the Main Window.

2. In the Universe Panel of the Simulation Window, click "Select PDB File..." and select N2.pdb.

3. Select GAFF from the Force Field dropdown menu.

4. Click the "Select File..." button next to the label that says "FF Mod File (optional)". Select N2.frcmod.

5. Click Load Universe. The N2 molecule will appear in the Chimera window.

6. For a better view, in the Chimera window, click on Actions --> Atoms/Bonds --> Ball & Stick.

7. In the Recipe Panel of the Simulation Window, click the "Load Recipe" button. Select recipe.json.

8. Scroll down to the bottom of the Simulation Window. Click "Run Next Ingredient" or "Run All Ingredients". Our recipe only has one ingredient, so the buttons are equivalent in this case.

9. When the simulation is complete (a few seconds), the trajectory will be loaded for viewing. Use the Trajectory Viewer Window to play the movie. The plot of energy vs. frame will also be displayed.

10. To measure the bond length vs. frame, click the "Structure Measurements" button in the Trajectory Viewer Window. Clicking the button will display Chimera's Structure Measurements Window.

11. Next, hold down ctrl and shift on your keyboard and click on the pair of atoms in the Chimera's main window. An outline will appear around each atom if the selection process was successful.

12. After selecting the atoms, click the "Create" button in the Distances tab of the Structure Measurements Window. If successful, a new entry will appear in the list of distances.

13. In the Sophia Trajectory Viewer Window, check the "Bonds" box in the Plotting Options Panel. A new plot will appear which shows the distance between the two atoms you selected vs. frame number.

14. In the Sophia Main Window, click the "Save Simulation" button. Select an empty directory to hold the saved files.

15. Use Excel or your data analysis program of choice to plot bond length vs. time and velocity vs. time.

16. You can get bond length vs. frame number from epoch00_bonds.csv, and you can convert frame number to time based on the time step you used (1 fs by default) and the output interval (1 frame by default).

17. To plot velocity, import epoch00_log.csv and compute velocity from the kinetic energy column.
